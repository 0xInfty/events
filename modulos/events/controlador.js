const service = require('./servicio')
const {logger} = require('../../logger')

async function create(req, res){
    try{
        const event = req.body
        logger.info({ action: 'Request to create event' })
        entryId = await service.createEvent(event)
        logger.info({ action: 'Event created' })
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end(JSON.stringify(entryId))
    } catch {
        logger.info({ action: 'Fail en event creation' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail en event creation"}))
    }
}

async function getAll(req, res){

    logger.info({ action: 'request to get events'})

    const events = await service.getEvents()
    console.log("controller: " + events)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(events))
}

async function search(req, res){

    logger.debug({ action: 'request to search event'})
    const searchString = req.body.searchString

    const objectsFound = await service.search(searchString)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

async function searchRecent(req, res){

    logger.debug({ action: 'request to search recent events'})
    const maxNumber = req.body.maxNumber

    const objectsFound = await service.searchRecent(maxNumber)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

module.exports = { create, getAll, search, searchRecent }