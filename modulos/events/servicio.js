const { ObjectID } = require("bson");
const { getDb } = require("../../mongodb");

const verbosityLevel = 1;
const consoleLogDebug = (stringToConsoleLog, importanceLevel=0) => {
    if ( importanceLevel <= verbosityLevel ) { console.log( stringToConsoleLog ) }
}

async function createEvent(event){ return new Promise(async (resolve, reject) => {
    
    event.created_ts = Date.now()
    const entryId = ObjectID()
    event._id = entryId
    const entryIdString = ObjectID(entryId).toString()
    consoleLogDebug(entryId, 1)

    await getDb().collection('events').insertOne(event).catch( error => {
        console.error(error);
        reject(error);
    })
    
    consoleLogDebug(entryIdString, 1)
    resolve( entryIdString )
})}

async function getEvents(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all events", 1);
    getDb().collection('events').find().toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "events" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function search( searchString ){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to search events, string: " + searchString, 1);
    getDb().collection('events').find({ $text: {$search: searchString}},{ score:{ $meta: "textScore"}}).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "events" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function searchRecent( maxNumber ) { return new Promise((resolve, reject) => {
    consoleLogDebug(`request to search the most recent ${maxNumber} events`, 1);
    getDb().collection('events').find().sort({"created_ts": -1}).limit(maxNumber).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "events" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

module.exports = { createEvent, getEvents, search, searchRecent }